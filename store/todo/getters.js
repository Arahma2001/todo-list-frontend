export default {
  finishedListId: function (state) {
    return state.list
      .filter(function (obj) {
        return obj.status == "finished";
      })
      .map(function (obj) {
        return obj.id;
      });
  },
};
