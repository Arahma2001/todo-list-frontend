export default {
  async fetchTodo({ commit }) {
    return new Promise(async (resolve, reject) => {
      await this.$axios
      .get("/todo")
      .then((response) => {
        commit("FETCH_TODO", response.data.data);
        resolve()
      })
      .catch((error) => {
        console.log(error.response);
        reject()
      });
    });
  },

  async storeTodo({ dispatch }, data) {
    return new Promise(async (resolve, reject) => {
      await this.$axios
        .post("/todo", data)
        .then(() => {
          dispatch("fetchTodo");
          resolve();
        })
        .catch(() => {
          reject();
        });
    });
  },

  async updateTodoStatus({ dispatch }, id) {
    return new Promise(async (resolve, reject) => {
      await this.$axios
        .patch("/todo/" + id)
        .then(() => {
          dispatch("fetchTodo");
          resolve();
        })
        .catch((error) => {
          console.log(error.response);
          reject();
        });
    });
  },

  async destroyTodo({ dispatch }, id) {
    return new Promise(async (resolve, reject) => {
      await this.$axios
        .delete("/todo/" + id)
        .then(() => {
          dispatch("fetchTodo");
          resolve();
        })
        .catch((error) => {
          console.log(error.response);
          reject();
        });
    });
  },
};
